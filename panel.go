package main

import (
	"database/sql"
	"flag"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	_ "net/http/pprof"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

var (
	db  *sql.DB
	err error
	//dbname    string = "production.db3"
	dbname    string = "host=localhost user=postgres password='NZtdvpWY3zV2LXmP' dbname=scan sslmode=disable"
	secret           = "f2iDMnkxp2NREM8idustxFKfBl3KTPTGWnop5RPHwES2KJrxRbfM7XMKxvda4Q4N"
	regexIPv4        = `(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})`
	regexIPv6        = `((?:[0-9A-Fa-f]{1,4}))((?::[0-9A-Fa-f]{1,4}))*::((?:[0-9A-Fa-f]{1,4}))((?::[0-9A-Fa-f]{1,4}))*|((?:[0-9A-Fa-f]{1,4}))((?::[0-9A-Fa-f]{1,4})){7}`
	username  string
	password  string
	notls     bool
)

func init() {
	rand.Seed(time.Now().UnixNano())
	flag.StringVar(&username, "u", "user", "username")
	flag.StringVar(&password, "p", "m1OPwXADP5gCXXbJWuDl9uLc", "password")
	flag.BoolVar(&notls, "notls", false, "")
	flag.Parse()
	_, err := os.Stat("./callbacks")
	if os.IsNotExist(err) {
		if err := os.Mkdir("./callbacks", os.ModePerm); err != nil {
			log.Fatal("error create callbacks dir", err.Error())
		}
	}
}

func main() {
	db, err = sql.Open("postgres", dbname)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS scans (id SERIAL, name TEXT, complete INTEGER);
	CREATE TABLE IF NOT EXISTS targets (id SERIAL, name TEXT, ip TEXT, workers INTEGER, timeout INTEGER, include TEXT, exclude TEXT, scaned INTEGER);
	CREATE TABLE IF NOT EXISTS port_scans (id SERIAL, name TEXT, ip TEXT, ports TEXT, timescan TEXT, read INTEGER);
	CREATE TABLE IF NOT EXISTS callbacks(id SERIAL, name TEXT, onTarget TEXT, about TEXT);
	CREATE TABLE IF NOT EXISTS callbacks_result (id SERIAL, cb_name TEXT, ip TEXT, port TEXT, time TEXT, output TEXT, error TEXT);
	CREATE TABLE IF NOT EXISTS unknown(id SERIAL, scanname TEXT, ip TEXT, port TEXT);
	CREATE TABLE IF NOT EXISTS notify(id SERIAL, ip TEXT, port TEXT, message TEXT, type TEXT, read INTEGER);
	CREATE TABLE IF NOT EXISTS ScanResult(
		id SERIAL,
		Name TEXT,
		Ip TEXT,
		Port INTEGER,
		Component TEXT,
		DeviceType TEXT,
		Hostname TEXT,
		OS TEXT,
		ProbeName TEXT,
		ProductName TEXT,
		Version TEXT);`)
	if err != nil {
		log.Fatal(err)
	}
	/*
		gin.SetMode(gin.ReleaseMode)
		gin.DisableConsoleColor()
		logfile, err := os.OpenFile("/tmp/scan.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, os.ModePerm)
		if err != nil {
			log.Fatal(err)
		}
		defer logfile.Close()
		gin.DefaultWriter = io.MultiWriter(logfile)
	*/
	r := gin.Default()
	r.Static("ui/assets", "./static/assets")
	r.LoadHTMLGlob("./static/templates/*")

	auth := r.Group("/ui", gin.BasicAuthForRealm(gin.Accounts{
		username: password,
	}, "WHO ARE U???"))

	r.GET("/", func(c *gin.Context) { c.Redirect(http.StatusMovedPermanently, "/ui") })
	//
	r.GET("/api/pgetest", getTest)
	r.POST("/api/ppostest", postTest)
	r.GET("/p/version",)
	//

	r.GET("/api/pget/"+secret, portGet)
	r.POST("/api/ppost/"+secret, portPost)
	r.GET("/api/vget/"+secret, verGet)
	r.POST("/api/vpost/"+secret, verPost)
	r.GET("/api/unknown/"+secret, getUnknown)

	auth.GET("/", index)
	auth.GET("/list", getList)
	auth.GET("/results/:name", showScanInfo)
	auth.GET("/dl/:name", removeScan)
	auth.GET("/stop/:name", stopScan)
	auth.GET("/callbacks/new", gNewCallback)
	auth.GET("/callbacks/all", allCallbacks)
	auth.POST("/callbacks/new", pNewCallback)
	auth.GET("/search", gSearch)
	auth.POST("/search", searchInfo)
	auth.POST("/new", newScan)
	auth.GET("/cbdl/:name", deleteCB)
	auth.GET("/cbinfo/:name", showCb)
	auth.GET("/info/:ip", ipInfo)
	auth.GET("/vendor/:service", getVendorList)
	auth.GET("/calc", calcPage)
	auth.POST("/calc", calculateIPs)

	if notls {
		panic(r.Run(":8081"))
	}

	// openssl genrsa -out server.key 2048
	// openssl req -new -x509 -key server.key -out server.pem -days 1000
	panic(r.RunTLS(":8080", "server.pem", "server.key"))
}

type Fingerprint struct {
	Name     string   `json:"Name"`
	Response struct{} `json:"Response"`
	Target   struct {
		Host string `json:"Host"`
		Port int64  `json:"Port"`
	} `json:"Target"`
	TcpFinger struct {
		ApplicationComponent string `json:"ApplicationComponent"`
		DeviceType           string `json:"DeviceType"`
		Hostname             string `json:"Hostname"`
		Info                 string `json:"Info"`
		//MatchRegexString     string `json:"MatchRegexString"`
		OperatingSystem string `json:"OperatingSystem"`
		ProbeName       string `json:"ProbeName"`
		ProductName     string `json:"ProductName"`
		Service         string `json:"Service"`
		Version         string `json:"Version"`
	} `json:"TcpFinger"`
}
