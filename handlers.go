package main

import (
	"database/sql"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

func newScan(c *gin.Context) {
	name := c.PostForm("name")
	iplist := c.PostForm("iplist")
	include_ports := c.PostForm("include")
	exclude_ports := c.PostForm("exclude")
	if name == "" {
		c.String(200, "Не указано название скана")
		return
	}

	ips := strings.Split(iplist, "\r\n")

	if len(ips) < 1 {
		c.String(200, "Не указаны ip-адреса")
		return
	}

	threads := c.PostForm("threads")
	workers, err := strconv.Atoi(threads)
	if err != nil {
		c.String(200, "threads not number")
		return
	}

	tim := c.PostForm("timeout")
	timeout, err := strconv.Atoi(tim)
	if err != nil {
		c.String(200, "timeout not number")
		return
	}

	tx, err := db.Begin()
	if err != nil {
		c.String(200, err.Error())
		return
	}
	stmt, err := tx.Prepare("insert into targets (ip, name, workers, timeout, include, exclude, scaned) values($1,$2,$3,$4,$5,$6,$7)")
	if err != nil {
		c.String(200, "#47 "+err.Error())
	}
	defer stmt.Close()

	for _, ip := range ips {
		if strings.Contains(ip, "#") {
			continue
		}
		if ip != "" {
			_, err = stmt.Exec(ip, name, workers, timeout, include_ports, exclude_ports, 0)
			if err != nil {
				tx.Rollback()
				c.String(200, "Error #59 "+err.Error())
				return
			}
		} else {
			_, err = stmt.Exec(ip, name, workers, timeout, include_ports, exclude_ports, 0)
			if err != nil {
				tx.Rollback()
				c.String(200, "Error #66 "+err.Error())
				return
			}
		}
	}
	tx.Commit()
	c.Redirect(302, "/ui")
}

func gSearch(c *gin.Context) {
	var service []string
	rows, err := db.Query("select distinct ProductName from scanresult;")
	if err != nil {
		c.String(200, "#79 "+err.Error())
		return
	}
	defer rows.Close()

	for rows.Next() {
		var s string
		rows.Scan(&s)
		service = append(service, s)
	}
	c.HTML(200, "search.html", gin.H{
		"service": service,
	})
}

func pNewCallback(ctx *gin.Context) {
	onTarget := ctx.PostForm("service")
	callback := ctx.PostForm("callback")
	about := ctx.PostForm("about")
	if onTarget == "" || callback == "" {
		ctx.String(200, "#99 empty")
		return
	}
	if err := registerCallback(onTarget, callback, about, db); err != nil {
		ctx.String(200, "#103 "+err.Error())
		return
	}
	ctx.Redirect(302, "/ui/callbacks/new")
}

func gNewCallback(c *gin.Context) {
	c.HTML(200, "callbacks.html", gin.H{
		"service": all_services_list,
	})
}

func allCallbacks(ctx *gin.Context) {
	rows, err := db.Query("SELECT name, onTarget, about FROM callbacks;")
	if err != nil {
		ctx.String(200, err.Error())
	}
	defer rows.Close()
	var CbInfo = []*CallbackInfo{}

	for rows.Next() {
		var cb = &CallbackInfo{}
		if err := rows.Scan(&cb.Name, &cb.Target, &cb.About); err != nil {
			ctx.String(200, err.Error())
			continue
		}
		CbInfo = append(CbInfo, cb)
	}
	ctx.HTML(200, "listcallbacks.html", gin.H{
		"cbs": CbInfo,
	})
}

func reScan(ctx *gin.Context) {
	name := ctx.Param("name")
	if name == "" {
		ctx.String(200, "empty request")
		return
	}
	_, err := db.Exec("UPDATE target SET scaned=0 WHERE name = $1", name)
	if err != nil {
		ctx.String(200, err.Error())
		return
	}
	ctx.Redirect(http.StatusMovedPermanently, "/ui/list")
}

func stopScan(ctx *gin.Context) {
	name := ctx.Param("name")
	if name == "" {
		ctx.String(200, "empty request")
		return
	}
	_, err := db.Exec("UPDATE target SET scaned=1 WHERE name = $1", name)
	if err != nil {
		ctx.String(200, err.Error())
		return
	}
	ctx.Redirect(http.StatusMovedPermanently, "/ui/list")
}

func removeScan(ctx *gin.Context) {
	name := ctx.Param("name")
	if name == "" {
		ctx.String(200, "empty request")
		return
	}
	_, err := db.Exec("DELETE FROM targets WHERE name = $1", name)
	if err != nil {
		ctx.String(200, err.Error())
		return
	}
	ctx.Redirect(http.StatusMovedPermanently, "/ui/list")
}

func getList(c *gin.Context) {
	rows, err := db.Query("SELECT DISTINCT ProductName FROM scantargets;")
	if err != nil {
		c.String(200, err.Error())
		return
	}
	type R struct {
		Name       string
		Total      string
		Scaned     string
		Discovered string
		Unknown    string
	}

	var Results []*R
	defer rows.Close()
	for rows.Next() {
		r := &R{}
		var n string
		rows.Scan(&n)
		r.Name = n

		_row := db.QueryRow("select count(*) from targets where name = $1", n)
		var count string
		if err := _row.Scan(&count); err != nil {
			panic(err)
		}
		r.Total = count

		_row = db.QueryRow("select count(*) from targets where name = $1 and scaned = 1", n)
		if err := _row.Scan(&count); err != nil {
			panic(err)
		}
		r.Scaned = count
		_row = db.QueryRow("select count(*) from results where scanname = $1 and not name = 'unknown'", n)
		if err := _row.Scan(&count); err != nil {
			panic(err)
		}
		r.Discovered = count

		_row = db.QueryRow("select count(*) from results where scanname = $1 and name = 'unknown'", n)
		if err := _row.Scan(&count); err != nil {
			panic(err)
		}
		r.Unknown = count

		Results = append(Results, r)
	}

	c.HTML(200, "list.html", gin.H{
		"Names": Results,
	})
}

func index(c *gin.Context) {
	c.HTML(200, "index.html", gin.H{})
}

func verGet(c *gin.Context) {
	row := db.QueryRow("select name, ip, ports from port_scans where read = 0")
	if row.Err() == nil {
		var name, ip, ports string
		row.Scan(&name, &ip, &ports)
		var r = make(map[string]string)
		r["name"] = name
		r["ip"] = ip
		r["ports"] = ports
		/* @audit */
		_, err = db.Exec("update port_scans set read = 1 where ip = $1", ip)
		if err != nil {
			log.Println(err)
			return
		}
		/**/
		c.JSON(200, r)
		return
	} else {
		c.String(200, row.Err().Error())
		return
	}
}

func portGet(c *gin.Context) {
	row := db.QueryRow("select name,ip,workers,timeout,include,exclude from targets where scaned = 0 limit 1")
	if row.Err() == nil {
		var name, ip, inc, exc string
		var workers, timeout int
		row.Scan(&name, &ip, &workers, &timeout, &inc, &exc)
		var r = make(map[string]interface{})
		_, err = db.Exec("update targets set scaned=1 where ip=$1", ip)
		if err != nil {
			log.Println(err)
			return
		}
		r["name"] = name
		r["target"] = ip
		r["workers"] = workers
		r["timeout"] = timeout
		r["include"] = inc
		r["exclude"] = exc
		c.JSON(200, r)
		return
	} else {
		c.String(200, row.Err().Error())
		return
	}
}

func portPost(ctx *gin.Context) {
	psr := &PortScanResponse{}
	ctx.BindJSON(psr)
	fmt.Printf("%#v\n", psr)
	ports := arrayToString(psr.Port, ",")
	_, err := db.Exec("insert into port_scans (name, ip, ports, timescan, read) values ($1,$2,$3,$4,$5)", psr.Name, psr.IP, ports, time.Now().Unix(), 0)
	if err != nil {
		log.Printf("%#v\n", err)
	}
}

func verPost(ctx *gin.Context) {
	verInfo := &Fingerprint{}
	ctx.BindJSON(verInfo)
	//go execCallbacks(verInfo)
	fmt.Printf("%s -> %s\n", verInfo.Name, verInfo.Target.Host)
	if verInfo.Target.Host != "" {
		_, err := db.Exec("INSERT INTO ScanResult (Name, Ip, Port, Component, DeviceType, Hostname, OS, ProbeName, ProductName, Version) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) ", verInfo.Name, verInfo.Target.Host, verInfo.Target.Port, verInfo.TcpFinger.ApplicationComponent, verInfo.TcpFinger.DeviceType, verInfo.TcpFinger.Hostname, verInfo.TcpFinger.OperatingSystem, verInfo.TcpFinger.ProbeName, verInfo.TcpFinger.ProductName, verInfo.TcpFinger.Version)
		if err != nil {
			panic(err)
			os.Exit(1)
		}
	} else {
		return
		_, err := db.Exec("INSERT INTO unknown (scanname, ip, port) VALUES ($1,$2,$3)", verInfo.Name, verInfo.Target.Host, verInfo.Target.Port)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

func showScanInfo(ctx *gin.Context) {
	scanName := ctx.Param("name")
	rows, err := db.Query("select ip, ports, timescan from port_scans where name = $1", scanName)
	if err != nil {
		ctx.String(200, err.Error())
		return
	}
	defer rows.Close()
	var info = []ScanInfo{}
	for rows.Next() {
		var ip, ports, scantime, servicename string
		if err := rows.Scan(&ip, &ports, &scantime); err == nil {

			pports := strings.Split(ports, ",")
			for _, port := range pports {
				i := &ScanInfo{}
				i.Ip = ip
				i.Port = port
				i.Scantime = convTime(scantime)
				row := db.QueryRow("SELECT name FROM results WHERE ip = $1 and port = $2", ip, port)
				row.Scan(&servicename)
				i.Service = servicename
				info = append(info, *i)
			}
		}
	}
	ctx.HTML(200, "scaninfo.html", gin.H{
		"tname": scanName,
		"info":  info,
	})
}

func searchInfo(ctx *gin.Context) {
	service := ctx.PostForm("service")
	vendor := ctx.PostForm("vendor")
	if vendor == "<undefined>" {
		vendor = ""
	}
	var (
		rows *sql.Rows
		err  error
	)
	/*
		CREATE TABLE IF NOT EXISTS ScanResult( id SERIAL, Name TEXT, Ip TEXT, Port INTEGER, Component TEXT, DeviceType TEXT, Hostname TEXT, OS TEXT, ProbeName TEXT, ProductName TEXT, Version TEXT);
	*/
	if vendor == "ALL" {
		rows, err = db.Query("SELECT Name, Ip, Port, Component, DeviceType, Hostname, OS, ProductName, Version FROM scanresult WHERE ProductName = $1", service)
		if err != nil {
			ctx.String(200, err.Error())
		}
	} else {
		rows, err = db.Query("SELECT Name, Ip, Port, Component, DevicyType, Hostname, OS, ProductName, Version FROM results WHERE ProbeName=$1 and ProductName=$2", service, vendor)
	}
	//defer rows.Close()
	var result = []*Fingerprint{}
	for rows.Next() {
		v := &Fingerprint{}
		if err := rows.Scan(&v.Name, &v.Target.Host, &v.Target.Port, &v.TcpFinger.ApplicationComponent, &v.TcpFinger.DeviceType, &v.TcpFinger.Hostname, &v.TcpFinger.OperatingSystem, &v.TcpFinger.ProductName, &v.TcpFinger.Version); err != nil {
			ctx.String(200, "%s\n", err.Error())
			continue
		}
		result = append(result, v)
	}
	rows.Close()
	ctx.HTML(200, "searchresult.html", gin.H{
		"service": service,
		"result":  result,
	})
}

func deleteCB(ctx *gin.Context) {
	name := ctx.Param("name")
	if _, err := db.Exec("DELETE FROM callbacks WHERE name = $1", name); err != nil {
		ctx.String(200, err.Error())
		return
	}
	os.Remove(filepath.Join("callbacks", name))
	ctx.Redirect(http.StatusMovedPermanently, "/ui/callbacks/all")
}

func showCb(ctx *gin.Context) {
	name := ctx.Param("name")
	src, err := ioutil.ReadFile(filepath.Join("callbacks", name))
	if err != nil {
		ctx.String(200, err.Error())
		return
	}
	var about string
	if err := db.QueryRow("SELECT about FROM callbacks WHERE name = $1", name).Scan(&about); err != nil {
		ctx.String(200, err.Error())
		return
	}
	ctx.HTML(200, "cbinfo.html", gin.H{
		"src":   src,
		"about": about,
	})
}

func ipInfo(ctx *gin.Context) {
	var info = []*IpInfo{}
	ip := ctx.Param("ip")
	rows, err := db.Query("SELECT port, name, vendor, version FROM results WHERE ip=$1", ip)
	if err != nil {
		ctx.String(200, err.Error())
		return
	}
	defer rows.Close()
	for rows.Next() {
		i := &IpInfo{}
		rows.Scan(&i.Port, &i.Service, &i.Vendor, &i.Version)
		info = append(info, i)
	}
	ctx.HTML(200, "ipinfo.html", gin.H{
		"ip":   ip,
		"info": info,
	})
}

func getUnknown(ctx *gin.Context) {
	row := db.QueryRow("SELECT scanname, ip, port FROM results WHERE name='unknown' LIMIT 1")
	u := &Unk{}
	if err := row.Scan(&u.Name, &u.Ip, &u.Port); err != nil {
		log.Println("error select unknown target with: ", err.Error())
		return
	}
	_, err = db.Exec("DELETE FROM results WHERE ip=$1 AND port=$2", u.Ip, u.Port)
	if err != nil {
		log.Println("error delete target with: ", err.Error())
		return
	}
	ctx.JSON(200, u)
}

func getVendorList(ctx *gin.Context) {
	service := ctx.Param("service")
	rows, err := db.Query("SELECT DISTINCT vendor FROM results WHERE name=$1", service)
	if err != nil {
		ctx.JSON(200, nil)
	}
	defer rows.Close()
	var vendors []string
	for rows.Next() {
		var s string
		rows.Scan(&s)
		vendors = append(vendors, s)
	}
	var result = make(map[string][]string)
	result[service] = vendors
	ctx.JSON(200, result)
}

func createNotify(ctx *gin.Context) {
	n := &Notify{}
	ctx.BindJSON(n)
}

func postTest(ctx *gin.Context) {
	type PSR struct {
		Name string `json:"Name"`
		IP   string `json:"IP"`
		Port string `json:"Port"`
	}
	psr := &PSR{}
	ctx.BindJSON(psr)
	fmt.Printf("%#v\n", psr)
}

func getTest(ctx *gin.Context) {
	row := db.QueryRow("select name,ip,workers,timeout,include,exclude from targets where scaned = 0 limit 1")
	if row.Err() == nil {
		var name, ip, inc, exc string
		var workers, timeout int
		row.Scan(&name, &ip, &workers, &timeout, &inc, &exc)
		var r = make(map[string]interface{})
		r["name"] = name
		r["target"] = ip
		r["workers"] = workers
		r["timeout"] = timeout
		r["include"] = inc
		r["exclude"] = exc
		ctx.JSON(200, r)
		return
	} else {
		ctx.String(200, row.Err().Error())
		return
	}
}

func calcPage(ctx *gin.Context) {
	ctx.HTML(200, "calc.html", gin.H{})
}

func calculateIPs(ctx *gin.Context) {
	ranges := ctx.PostForm("ips")
	var count int

	for _, iprange := range strings.Split(ranges, "\r\n") {
		fmt.Println(iprange, count)
		if strings.Contains(iprange, "#") {
			continue
		}
		_, subnet, err := net.ParseCIDR(iprange)
		if err != nil {
			continue
		}
		mask := binary.BigEndian.Uint32(subnet.Mask)
		start := binary.BigEndian.Uint32(subnet.IP)
		finish := (start & mask) | (mask ^ 0xffffffff)
		for i := start; i <= finish; i++ {
			count++
		}
	}
	ctx.String(200, fmt.Sprintf("Количество ипов в списке: %d\n", count))
	return
}

func portScanVersion(ctx *gin.Context) {
	ctx.JSON(200, map[string]string{"v": "0.2"})
}

func xmppNotify(msg string) {}

type Notify struct {
	Ip      string `json:"ip"`
	Port    string `json:"port"`
	Message string `json:"message"`
	Type    string `json:"type"` // vulnerable, info, etc
	Send    bool   `json:"send_notify"`
}

type Unk struct {
	Name string `json:"name"`
	Ip   string `json:"ip"`
	Port string `json:"port"`
}

type IpInfo struct {
	Port    string
	Service string
	Vendor  string
	Version string
}
