package main

import (
	"bytes"
	"crypto/tls"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func Hosts(cidr string) ([]string, int, error) {
	ip, ipnet, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, 0, err
	}

	var ips []string
	for ip := ip.Mask(ipnet.Mask); ipnet.Contains(ip); inc(ip) {
		ips = append(ips, ip.String())
	}

	lenIPs := len(ips)
	switch {
	case lenIPs < 2:
		return ips, lenIPs, nil

	default:
		return ips[1 : len(ips)-1], lenIPs - 2, nil
	}
}

func inc(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

func registerCallback(target, src, about string, db *sql.DB) error {
	name := randStr(32)
	f, err := os.OpenFile(filepath.Join("./callbacks", name), os.O_CREATE|os.O_APPEND|os.O_WRONLY, os.ModePerm)
	if err != nil {
		return err
	}
	src = strings.ReplaceAll(src, "\r", "")
	defer f.Close()
	if _, err := f.WriteString(src); err != nil {
		return err
	}
	_, err = db.Exec("insert into callbacks (name, onTarget, about) values($1, $2, $3)", name, target, about)
	return err
}

func execCallbacks(service *VersionScanResponse) {
	rows, err := db.Query("select name from callbacks where onTarget = $1", service.Name)
	if err != nil {
		return
	}
	defer rows.Close()
	var cb []string
	for rows.Next() {
		var c string
		if err := rows.Scan(&c); err != nil {
			continue
		}
		cb = append(cb, c)
	}
	for _, i := range cb {
		go execCmd(i, fmt.Sprintf("%s:%d", service.IP, service.Port))

		/*
			resp, err := execCmd(i, fmt.Sprintf("%s:%d", service.IP, service.Port))
			if err != nil {
				db.Exec("insert into callbacks_result (cb_name, ip, port, error) values (?,?,?,?)", i, service.IP, service.Port, err.Error())
			}
			db.Exec("insert into callbacks_result (cb_name, ip, port, output) values (?,?,?,?)", i, service.IP, service.Port, resp)
		*/
	}
}

func execCmd(cName, arg string) (string, error) {
	cmd := exec.Command(cName, arg)
	var outb, errb bytes.Buffer
	cmd.Stdout = &outb
	cmd.Stderr = &errb
	err := cmd.Run()
	if err != nil {
		return "", err
	}
	if outb.String() != "" {
		return outb.String(), nil
	}
	return errb.String(), err
}

func arrayToString(a []int, delim string) string {
	return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delim, -1), "[]")
}

func randStr(n int) string {
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyz")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func convTime(unix string) string {
	i, err := strconv.ParseInt(unix, 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(i, 0)
	return tm.String()
}

func certInfo() {
	conf := &tls.Config{
		InsecureSkipVerify: true,
	}

	conn, err := tls.Dial("tcp", "www.google.com:443", conf)
	if err != nil {
		log.Println("Error in Dial", err)
		return
	}
	defer conn.Close()
	certs := conn.ConnectionState().PeerCertificates
	for _, cert := range certs {
		fmt.Printf("Issuer Name: %s\n", cert.Issuer)
		fmt.Printf("Expiry: %s \n", cert.NotAfter.Format("2006-January-02"))
		fmt.Printf("Common Name: %s \n", cert.Issuer.CommonName)
		fmt.Println(cert.DNSNames)

	}
}

type PortScanResponse struct {
	Name string `json:"Name"`
	IP   string `json:"IP"`
	Port []int  `json:"Ports"`
}

type VersionScanResponse struct {
	Scanname        string `json:"Scanname"`
	Banner          string `json:"Banner"`
	Cpe             string `json:"CPE"`
	DeviceType      string `json:"DeviceType"`
	Error           string `json:"Error"`
	Hostname        string `json:"Hostname"`
	IP              string `json:"IP"`
	Info            string `json:"Info"`
	Name            string `json:"Name"`
	OperatingSystem string `json:"OperatingSystem"`
	Port            int64  `json:"Port"`
	Protocol        string `json:"Protocol"`
	ServiceURL      string `json:"ServiceURL"`
	Sign            string `json:"Sign"`
	StatusCode      int64  `json:"StatusCode"`
	VendorProduct   string `json:"VendorProduct"`
	Version         string `json:"Version"`
}

type ScanInfo struct {
	Ip       string
	Port     string
	Scantime string
	Service  string
}

type CallbackInfo struct {
	Name   string
	Target string
	About  string
}
