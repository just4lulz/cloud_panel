module cloudscan

go 1.16

require (
	github.com/GeertJohan/go.rice v1.0.2 // indirect
	github.com/Ullaakut/nmap/v2 v2.1.1 // indirect
	github.com/alitto/pond v1.5.0 // indirect
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394 // indirect
	github.com/gin-contrib/static v0.0.1 // indirect
	github.com/gin-gonic/gin v1.7.2
	github.com/go-ping/ping v0.0.0-20210506233800-ff8be3320020 // indirect
	github.com/lcvvvv/gonmap v0.1.17 // indirect
	github.com/lib/pq v1.10.2
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/pkg/profile v1.6.0
	github.com/projectdiscovery/wappalyzergo v0.0.7 // indirect
	github.com/rverton/webanalyze v0.3.3 // indirect
)
